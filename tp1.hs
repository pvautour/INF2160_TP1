-- Formule nettoyant un string entré et retournant un Int
-- @input hangeul en String
-- @output hangeul en Int
clean :: String -> Int
clean xs = read([x | x <- xs, not (x `elem` " ")])

-- Formule nettoyant les valeurs entrées et retournant une liste Int
-- @input phrase de hangeul en String
-- @output Phrase en liste de Int
cropPhrase :: String -> [Int]
cropPhrase x = map clean (words x)


-- Formule calculant le premier jamos à partir de son hageul
calcci :: Int -> Int
-- @input hangeul en Int
-- @output premier jamos
calcci n = quot (n-44032) 588

-- Formule calculant le deuxième jamos à partir de son hageul
calcv :: Int -> Int
-- @input hangeul en Int
-- @output deuxieme jamos
calcv n = quot (n-(44032 + 588 * calcci(n))) 28

-- Formule calculant le troisième jamos à partir de son hageul
-- @input hangeul en Int
-- @output troisieme jamos
calcf :: Int -> Int
calcf n = n-(44032 + 588 * calcci(n) + 28 * calcv(n))

-- Fonction traduisant une valeur numérique en tupple selon la formule de traduction
-- @input hangeul en Int
-- @output hangeul traduit en tuple de jamos
asTuple :: Int -> (Int,Int,Int)
asTuple 00000 = (19,21,80)
asTuple n = ((calcci n), (calcv n), (calcf n))

-- Fonction traduisant une liste de valeurs numériques en tuples selon la formule de traduction
-- @input liste de hangeuls jamos
-- @output hangeuls en tuples traduits par jamos
tuplePhrase :: [Int] -> [(Int,Int,Int)]
tuplePhrase xs = map asTuple xs

-- Fonction traduissant un string entré en liste de tuples
-- @input liste de hangeuls en string
-- @output hangeuls traduit en tuple de jamos
lecture :: String -> [(Int,Int,Int)]
lecture x = tuplePhrase (cropPhrase(x))

-- Fonction prennant en argument une liste de tuples et leur appliquant les ajustements basés sur leurs suivants
-- @input liste de jamos en tuple
-- @output liste de jamos ajustes
ajustement:: [(Int, Int,Int)] -> [(Int, Int,Int)]
ajustement xs = (map ajustement' (zip xs (tail xs))) ++ [(last xs)]

-- Fonction comparant un tuple et son suivant et retournant sa version ajustée
-- @input jamos en tuple et son suivant
-- @output premier jamos ajuste par rapport a son suivant
ajustement':: ((Int, Int,Int),(Int, Int,Int)) -> (Int, Int,Int)
ajustement' ((a,b,1),(11,c,d)) = (a,b,28)
ajustement' ((a,b,1),(2,c,d)) = (a,b,29)
ajustement' ((a,b,1),(5,c,d)) = (a,b,30)
ajustement' ((a,b,1),(6,c,d)) = (a,b,31)
ajustement' ((a,b,1),(15,c,d)) = (a,b,32)

ajustement' ((a,b,4),(0,c,d)) = (a,b,33)
ajustement' ((a,b,4),(5,c,d)) = (a,b,34)

ajustement' ((a,b,7),(11,c,d)) = (a,b,35)
ajustement' ((a,b,7),(2,c,d)) = (a,b,36)
ajustement' ((a,b,7),(5,c,d)) = (a,b,37)
ajustement' ((a,b,7),(6,c,d)) = (a,b,38)
ajustement' ((a,b,7),(16,c,d)) = (a,b,39)

ajustement' ((a,b,8),(11,c,d)) = (a,b,40)
ajustement' ((a,b,8),(2,c,d)) = (a,b,41)
ajustement' ((a,b,8),(5,c,d)) = (a,b,42)

ajustement' ((a,b,16),(5,c,d)) = (a,b,43)

ajustement' ((a,b,17),(11,c,d)) = (a,b,44)
ajustement' ((a,b,17),(2,c,d)) = (a,b,45)
ajustement' ((a,b,17),(5,c,d)) = (a,b,46)
ajustement' ((a,b,17),(6,c,d)) = (a,b,47)
ajustement' ((a,b,17),(17,c,d)) = (a,b,48)

ajustement' ((a,b,19),(11,c,d)) = (a,b,49)
ajustement' ((a,b,19),(2,c,d)) = (a,b,50)
ajustement' ((a,b,19),(5,c,d)) = (a,b,51)
ajustement' ((a,b,19),(6,c,d)) = (a,b,52)
ajustement' ((a,b,19),(16,c,d)) = (a,b,53)

ajustement' ((a,b,21),(11,c,d)) = (a,b,54)
ajustement' ((a,b,21),(5,c,d)) = (a,b,55)

ajustement' ((a,b,22),(11,c,d)) = (a,b,56)
ajustement' ((a,b,22),(2,c,d)) = (a,b,57)
ajustement' ((a,b,22),(5,c,d)) = (a,b,58)
ajustement' ((a,b,22),(6,c,d)) = (a,b,59)
ajustement' ((a,b,22),(16,c,d)) = (a,b,60)

ajustement' ((a,b,23),(11,c,d)) = (a,b,61)
ajustement' ((a,b,23),(2,c,d)) = (a,b,62)
ajustement' ((a,b,23),(5,c,d)) = (a,b,63)
ajustement' ((a,b,23),(6,c,d)) = (a,b,64)
ajustement' ((a,b,23),(16,c,d)) = (a,b,65)

ajustement' ((a,b,25),(2,c,d)) = (a,b,66)
ajustement' ((a,b,25),(5,c,d)) = (a,b,67)
ajustement' ((a,b,25),(6,c,d)) = (a,b,68)
ajustement' ((a,b,25),(16,c,d)) = (a,b,69)

ajustement' ((a,b,27),(11,c,d)) = (a,b,70)
ajustement' ((a,b,27),(0,c,d)) = (a,b,71)
ajustement' ((a,b,27),(2,c,d)) = (a,b,72)
ajustement' ((a,b,27),(3,c,d)) = (a,b,73)
ajustement' ((a,b,27),(5,c,d)) = (a,b,74)
ajustement' ((a,b,27),(6,c,d)) = (a,b,75)
ajustement' ((a,b,27),(7,c,d)) = (a,b,76)
ajustement' ((a,b,27),(9,c,d)) = (a,b,77)
ajustement' ((a,b,27),(12,c,d)) = (a,b,78)
ajustement' ((a,b,27),(18,c,d)) = (a,b,79)

ajustement' ((a,b,c),(_,_,_)) = (a,b,c)

-- Fonction prennant en argument une liste de tupples et les traduissant en phrases.
-- @input liste de jamos en tuple
-- @output jamos traduits
traduireTuples :: [(Int,Int,Int)] -> String
traduireTuples xs = concat (map traduire' xs)

-- Fonction prennant en argument un tuple numérique et le traduisant en lettres
-- @input jamos en tuple
-- @output jamos traduit
traduire':: (Int,Int,Int) -> String
traduire' (x,y,z) = traduire1' x ++ traduire2' y ++ traduire3' z

-- Fonction prennant en argument une valeur numérique associée au premier ensemble de lettres d'un tuple représentant un mot et retournant les lettres qui y sont associées.
-- @input jamos en valeur numerique 
-- @output jamos 1
-- @error si on a une valeur negative ou qui sort du tableau
traduire1' :: Int -> String
traduire1' index = ["g","kk","n","d","tt","r","m","b","pp","s","ss","","j","jj","ch","k","t","p","h",". "] !! index

-- Fonction prennant en argument une valeur numérique associée au deuxième ensemble de lettres d'un tuple représentant un mot et retournant les lettres qui y sont associées.
-- @input jamos en valeur numerique 
-- @output jamos 2
-- @error si on a une valeur negative ou qui sort du tableau
traduire2' :: Int -> String
traduire2' index = ["a","ae","ya","yae","eo","e","yeo","ye","o","wa","wae","oe","yo","u","wo","we","wi","yu","eu","ui","i",""] !! index

-- Fonction prennant en argument une valeur numérique associée au troisième ensemble de lettres d'un tuple représentant un mot et retournant les lettres qui y sont associées.
-- @input jamos en valeur numerique 
-- @output jamos 3
-- @error si on a une valeur negative ou qui sort du tableau
traduire3' :: Int -> String
traduire3' index = ["","k","k","kt","n","nt","nh","t","l","lk","lm","lp","lt","lt","lp","lh","m","p","pt","t","t","ng","t","t","k","t","p","h","g","ngn","ngn","ngm","k-k","n-g","ll","d","nn","nn","nm","t-t","r","ll","ll","mn","b","mn","mn","mm","p-p","s","nn","nn","nm","t-t","ng-","ngn","j","nn","nn","nm","t-t","ch","nn","nn","nm","t-t","nn","nn","nm","t-t","h","k","nn","t","nn","nm","p","hs","ch","t",""] !! index   

-- Traduit le texte
-- @input String contenant des hageuls sous forme numerique
-- @output Texte traduit
traduireTexte :: String -> String
traduireTexte p = traduireTuples(ajustement(lecture p))

-- Cette fonction prend en argument une phrase et remplace les charactere délimitants les mots par des espaces pour que words puisse ensuite être utilisé.
-- @input Array de characteres a nettoyer
-- @output Array de characteres sans '&', ';' ou '#'
parse' :: [Char] -> [Char]
parse' = concatMap (\c -> if c=='.' then " 00000" else if (c=='&' || c==';' || c=='#') then " " else [c])

-- Exécute le programme
-- @input String de characteres a traduire
-- @output Traduction du string entre
exectuer :: String -> String
exectuer s = traduireTexte (parse' s)

-- Prend en entree une suite de hageuls et retourne soit sa traduction, soit un message d'erreur
-- @input String de characteres a traduire
-- @output Message d'erreur si la string contient des characteres invalides, la traduction sinon
romaniser:: String -> String
romaniser s = if validerCharacteres s then exectuer s else "Erreur: entree invalide!"

-- Valide que ce qui est entre contient seulement des characteres valides
-- @input String de charactere a valider
-- @output False si invalide, True si valide
validerCharacteres:: String -> Bool
validerCharacteres s = length (filter estNombre (concat((words (parse' s))))) == length (concat(words (parse' s)))

-- Valide qu'un charactere est un chiffre
-- @input charactere a valider
-- @output False si invalide, True si valide
estNombre:: Char -> Bool
estNombre c  
    | c == '1' = True 
    | c == '2' = True 
    | c == '3' = True 
    | c == '4' = True 
    | c == '5' = True 
    | c == '6' = True 
    | c == '7' = True 
    | c == '8' = True 
    | c == '9' = True 
    | c == '0' = True 
    | otherwise = False


main::IO()
main = do s <- getLine 
          putStr ( romaniser s )


-- "&#44039; &#53844; &#46944; &#49240; &#53364; &#46944; &#45128; &#50976; &#50872; &#46612; &#51012; &#49240; &#47792; &#54028; &#50688; &#44256;"





